#!/bin/bash
# Script : mesh_buster binary launcher for SLT Environment
# Author : Sanjay Pottbatni (spottbatni@amperecomputing.com)
#

TAG="[MESH_BUSTER]:"
total_time=$1
time_each_per_opmode=`expr $total_time / 2`

echo "$TAG Test Executing in Even:Odd operation mode for $time_each_per_opmode seconds ..."
echo " "

(./mesh_buster_v2 -c 0 -s all -m 1024 -t $time_each_per_opmode -b 0)

echo "$TAG Test Executing in 1:N operation mode for $time_each_per_opmode seconds ..."
echo " "

(./mesh_buster_v2 -c 0 -s all -m 1024 -t $time_each_per_opmode -b 1)

#(./mesh_buster_v1 -c 0 -s all -m 2048 -d random -t $1)

