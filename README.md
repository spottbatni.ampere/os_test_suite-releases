# os_test_suite - Releases
This repo maintains the stable releases of various tests of os_test_suite in binary form and also includes script file to trigger it.

## Features
- Stable and verified releases of each tests with fedora and CentOS support.
- Script file for each test to launch in BLT and SLT environment.
- Commands to launch the test scripts

## Test binaries Names and details
- mesh_buster_v1 - This is binary of Mesh_Buster version 1 tests.
  The mesh buster (version-1) test sends write and read transactions with large amount of memory across all HNF node using Hash Algorithm

## Installation
Please execute commands to clone repo in your environment.
```sh
git clone https://gitlab.com/AmpereComputing/validation/cpu/os_test_suite-releases.git
cd os_test_suite-releases
```
## Steps to run each tests
mesh_buster (version-1) test command :
```sh
./run_meshbuster.sh <runtime (seconds)>
Ex.: ./run_meshbuster.sh 600 

```
## Roadmap
Here listed upcoming releases in future :
- [ ] mesh_buster (version-2)

## Author :
Sanjay Pottbatni (spottbatni@amperecomputing.com)

